import 'dart:async';
import 'package:flutter/material.dart';
import 'package:oven_timer/data/oven_products.dart';
import 'package:oven_timer/selectionscreen.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:audioplayers/audioplayers.dart';

class Level extends StatefulWidget {
  final int row;

  const Level({Key? key, required this.row}) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  State<Level> createState() => LevelState(rowNr: row);
}

class LevelState extends State<Level> {
  final player = AudioPlayer();
  bool active = false;
  OvenProduct? selectedProduct;
  Timer? timer;
  int? rowNr = -1;
  var counter = 0;
  int TotalTime = -1;

  LevelState({this.rowNr});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
      color: Color.fromARGB(10 + (10 * (rowNr! % 2)), 0, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          // Button and container
          Expanded(child: buildInformation()),
          buildButton(),
        ],
      ),
    );
  }

  Container buildInformation() {
    if (!active) {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        alignment: Alignment.center,
        child: Text("Oven laag $rowNr",
            style: const TextStyle(color: Colors.grey)),
      );
    } else {
      return itemInformation();
    }
  }

  Container itemInformation() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          productImageData(),
          progressIndicator(),
        ],
      ),
    );
  }

  StepProgressIndicator progressIndicator() {
    if ((counter <= 0) && ((counter % 10) == 0)) {
      player.play(AssetSource('sound/alarm-tone.mp3'));
    }

    if (counter > 0) {
      return StepProgressIndicator(
        totalSteps: TotalTime,
        currentStep: TotalTime - counter,
        size: 8,
        padding: 0,
        selectedColor: Colors.yellow,
        unselectedColor: Colors.cyan,
        roundedEdges: const Radius.circular(10),
        selectedGradientColor: const LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.yellowAccent, Colors.deepOrange],
        ),
        unselectedGradientColor: const LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.black, Colors.blue],
        ),
      );
    } else {
      return const StepProgressIndicator(
        totalSteps: 1,
        currentStep: 1,
        size: 8,
        padding: 0,
        selectedColor: Colors.green,
        unselectedColor: Colors.green,
        roundedEdges: Radius.circular(10),
        selectedGradientColor: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.green, Colors.green],
        ),
        unselectedGradientColor: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.green, Colors.green],
        ),
      );
    }
  }

  Row productImageData() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        productImage(),
        nameAndTimer(),
      ],
    );
  }

  Container productImage() {
    Image plch;
    if (selectedProduct!.image == null) {
      plch = Image.asset(
        "assets/images/scheflogo.png",
        width: 50,
      );
    } else {
      plch = Image.network(
        selectedProduct!.image!,
        width: 50,
      );
    }

    return Container(
      padding: const EdgeInsets.all(4),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: plch,
      ),
    );
  }

  Container nameAndTimer() {
    return Container(
      padding: const EdgeInsets.all(5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            selectedProduct!.name,
            style: const TextStyle(
              color: Colors.grey,
              fontSize: 15,
            ),
          ),
          Text(
            timeStringBuilder(counter),
            style: const TextStyle(
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }

  String timeStringBuilder(int timer) {
    String result = "";

    if (timer < 0) {
      result += "+  ";
    }

    timer = timer.abs();
    int minutes = 0;

    while (timer >= 60) {
      minutes++;
      timer -= 60;
    }
    result += minutes.toString();
    result += ":";

    if (timer <= 9) {
      result += "0";
    }
    result += timer.toString();

    return result;
  }

  CircleAvatar buildButton() {
    if (active) {
      //Timer is active, so we should show a stop button, in red.
      return CircleAvatar(
        radius: 25,
        backgroundColor: Colors.red,
        child: IconButton(
          onPressed: stopTimer,
          icon: const Icon(Icons.stop),
        ),
      );
    } else {
      return CircleAvatar(
        radius: 25,
        backgroundColor: const Color.fromARGB(255, 10, 239, 18),
        child: IconButton(
          onPressed: () {
            startTimer(context);
          },
          icon: const Icon(Icons.add),
        ),
      );
    }
  }

  void startTimer(BuildContext context) async {
    //Now we simulate a item being picked. This of course will be replaced by a selection screen.

    final OvenProduct userChoice = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const SelectionScreen(),
        ));

    setState(() {
      selectedProduct = userChoice;
      TotalTime = selectedProduct!.oventime200;
      active = true;
      counter = selectedProduct!.oventime200;
      //Set up alarm sound.
      player.setVolume(0.8);

      //Timer settings:
      timer = Timer.periodic(const Duration(seconds: 1), (_) {
        counter--;
        setState(() {});
      });
    });
  }

  void stopTimer() {
    player.stop();
    timer?.cancel();
    timer = null;
    selectedProduct = null;
    active = false;
    counter = 0;
    setState(() {});
  }
}
