import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:volume/volume.dart';

class VolumeWarning extends StatefulWidget {
  const VolumeWarning({Key? key}) : super(key: key);

  @override
  State<VolumeWarning> createState() => _VolumeWarningState();
}

class _VolumeWarningState extends State<VolumeWarning> {
  late AudioManager audioManager;
  late int maxVol = -1;
  late int currentVol = -1;
  ShowVolumeUI showVolumeUI = ShowVolumeUI.SHOW;

  @override
  void initState() {
    super.initState();
    audioManager = AudioManager.STREAM_MUSIC;
    initAudioStreamType();
  }

  Future<void> initAudioStreamType() async {
    await Volume.controlVolume(AudioManager.STREAM_MUSIC);
  }

  updateVolumes() async {
    maxVol = await Volume.getMaxVol;
    currentVol = await Volume.getVol;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    updateVolumes();

    if ((maxVol / currentVol) < 3) {
      if (maxVol == -1) {
        return Container();
      }
    }

    return Container(
      color: Colors.orangeAccent,
      alignment: Alignment.center,
      child: Row(
        children: [
          Expanded(flex: 4, child: noVolume()),
          Expanded(flex: 1, child: unMuteButton()),
        ],
      ),
    );
  }

  Column noVolume() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Text(
          "Volume staat niet aan!",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.black,
            letterSpacing: 1,
          ),
        ),
        Text(
          "Druk hier om het volume aan te zetten.",
          style: TextStyle(fontSize: 12),
        ),
      ],
    );
  }

  Container unMuteButton() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: ElevatedButton(
        onPressed: unmuteVolume,
        child: const Icon(Icons.notifications_on_outlined, size: 30),
      ),
    );
  }

  void unmuteVolume() async {
    await Volume.setVol(maxVol, showVolumeUI: showVolumeUI);
  }
}
