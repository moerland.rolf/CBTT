import 'package:flutter/material.dart';

import 'data/oven_products.dart';

// Create a stateful widget
class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  // Create the state that will be used by the widget
  @override
  _SettingsState createState() => _SettingsState();
}

// Create a state class
class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Settings:"),
      contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 70),
      content: SizedBox(
        width: double.maxFinite,
        child: ListView.builder(
          itemCount: products.length,
          itemBuilder: (BuildContext context, int index) {
            return itemCard(products[index], index);
          },
        ),
      ),
      actions: <Widget>[
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text("ok")),
      ],
    );
  }

  Container itemCard(OvenProduct product, int index) {
    int time = product.oventime200;
    int seconds = time % 60;
    int minutes = (time ~/ 60) % 60;

    return Container(
      color: Color.fromRGBO(10, 10, 10, ((index % 2) == 0) ? 0.1 : 0.2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(product.name),
          Expanded(child: Text("")),
          Text(minutes.toString()),
          Text(" M,  "),
          Text(seconds.toString()),
          Text(" S   "),
          iconButton(product, const Icon(Icons.add), 5),
          iconButton(product, const Icon(Icons.remove), -5),
        ],
      ),
    );
  }

  iconButton(OvenProduct product, Icon btnicon, int valueChange) {
    return FloatingActionButton.small(
      child: btnicon,
      onPressed: () {
        setState(() {
          product.oventime200 += valueChange;
        });
      },
    );
  }
}
