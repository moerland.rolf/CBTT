class OvenProduct {
  final String name;
  final String? image;
  int oventime180;
  int oventime200;

  OvenProduct(
      {required this.name,
      required this.oventime180,
      required this.oventime200,
      this.image});
}

List<OvenProduct> products = [
  //
  // OvenProduct(
  //     name: "",
  //     oventime180: ,
  //     oventime200: ,
  //     image: ""),
  //

  //==================================================================Brood=======================================================

  //Prokorn
  OvenProduct(
      name: "Prokorn",
      oventime180: 600,
      oventime200: 600,
      image:
          "https://static.hanos.com/sys-master/productimages/h5b/hc6/9730252570654/41205320_lev.jpg_914Wx914H"),

  //Flaquette
  OvenProduct(
      name: "Flaquette",
      oventime180: 600,
      oventime200: 600,
      image:
          "https://www.bakeplus.nl/static/uploads/pictures/normal/5413476988287.jpeg"),

  //Bagel
  OvenProduct(
      name: "Bagel",
      oventime180: 600,
      oventime200: 600,
      image:
          "https://www.broodjeskoningutrecht.nl/wp-content/uploads/2018/11/Bagel-Plain-Alt.jpg"),

  //==================================================================Hartige snacks=====================================================
  // Worstenbrood (ontdooid)
  OvenProduct(
      name: "Worstenbrood (ontdooid)",
      oventime180: 600,
      oventime200: 600,
      image:
          "https://houbenworstenbrood.nl/wp-content/uploads/2022/05/Traditioneel-Worstenbrood.png"),

  // Worstenbrood (bevroren)
  OvenProduct(
      name: "Worstenbrood (bevroren)",
      oventime180: 660,
      oventime200: 660,
      image:
          "https://houbenworstenbrood.nl/wp-content/uploads/2022/05/Traditioneel-Worstenbrood.png"),

  // Saucijzenbrood (ontdooid)
  OvenProduct(
      name: "Saucijzenbrood (ontdooid)",
      oventime180: 720,
      oventime200: 720,
      image:
          "https://www.chaupain.nl/wp-content/uploads/2015/07/3120-saucijs-rb.png"),

  // Saucijzenbrood (bevroren)
  OvenProduct(
      name: "Saucijzenbrood (bevroren)",
      oventime180: 900,
      oventime200: 900,
      image:
          "https://www.chaupain.nl/wp-content/uploads/2015/07/3120-saucijs-rb.png"),

  // Kaasbroodje
  OvenProduct(
      name: "Kaasbroodje",
      oventime180: 1500,
      oventime200: 1500,
      image:
          "https://versinn.nl/images/02634-Kaasbroodje-souffle-p-st-2019-1553767523.jpg"),

  // Old Amsterdam
  OvenProduct(
      name: "Old Amsterdam",
      oventime180: 600,
      oventime200: 600,
      image:
          "https://deboulanger.nl/wp-content/uploads/2021/06/Roule_jambon-fromage-300x300.jpg"),

  //==================================================================Zoete snacks=======================================================

  // Muffin (ontdooid)
  OvenProduct(
      name: "Muffin (ontdooid)",
      oventime180: 550,
      oventime200: 550,
      image:
          "https://crumbleandsass.nl/wp-content/uploads/2020/03/Bananen-Toffee-_-CrumbleSass-600x600.jpg"),

  // Muffin (bevroren)
  OvenProduct(
      name: "Muffin (bevroren)",
      oventime180: 700,
      oventime200: 700,
      image:
          "https://crumbleandsass.nl/wp-content/uploads/2020/03/Bananen-Toffee-_-CrumbleSass-600x600.jpg"),

  // Cookie
  OvenProduct(
      name: "Cookie",
      oventime180: 600,
      oventime200: 600,
      image:
          "https://assets.sainsburys-groceries.co.uk/gol/8102107/1/640x640.jpg"),

  // Appelflap
  OvenProduct(
      name: "Appelflap",
      oventime180: 780,
      oventime200: 780,
      image:
          "https://www.ahealthylife.nl/wp-content/uploads/2021/06/Appelflap_voedingswaarde-1-1.jpg"),
];
