import 'package:flutter/material.dart';
import 'package:oven_timer/data/oven_products.dart';

class SelectionScreen extends StatefulWidget {
  const SelectionScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _SelectionScreenState createState() => _SelectionScreenState();
}

class _SelectionScreenState extends State<SelectionScreen> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Keuzemenu:"),
      contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      insetPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 70),
      content: SizedBox(
        width: double.maxFinite,
        child: ListView.builder(
          itemCount: products.length,
          itemBuilder: (BuildContext context, int index) {
            return infoCard(products[index], context);
          },
        ),
      ),
      actions: <Widget>[
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text("Annuleer")),
      ],
    );
  }
}

Column infoCard(OvenProduct ovenProduct, BuildContext context) {
  Image plch;
  if (ovenProduct.image == null) {
    plch = Image.asset(
      "assets/images/scheflogo.png",
      height: 50,
    );
  } else {
    plch = Image.network(
      ovenProduct.image!,
      height: 50,
    );
  }
  return Column(
    children: [
      InkWell(
        child: Container(
          color: Colors.grey.shade200,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
            child: Row(
              children: [
                Expanded(flex: 1, child: plch),
                Expanded(flex: 3, child: Text(ovenProduct.name)),
              ],
            ),
          ),
        ),
        onTap: () {
          Navigator.of(context).pop(ovenProduct);
        },
      ),
      const Divider(
        color: Colors.grey,
        height: 2,
        thickness: 2,
      ),
    ],
  );
}
