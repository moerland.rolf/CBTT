import 'dart:js';

import 'package:flutter/material.dart';
import 'package:slide_digital_clock/slide_digital_clock.dart';
import 'settings.dart';

AppBar mainAppBar(BuildContext context) {
  return AppBar(
    title: const Text("Chef Beach's Tito's Timer"),
    actions: <Widget>[
      DigitalClock(
        areaWidth: 110,
        hourMinuteDigitDecoration: const BoxDecoration(border: null),
        secondDigitDecoration: const BoxDecoration(border: null),
        digitAnimationStyle: Curves.elasticInOut,
        is24HourTimeFormat: true,
        areaDecoration: const BoxDecoration(
          color: Colors.transparent,
        ),
        areaAligment: AlignmentDirectional.center,
        hourMinuteDigitTextStyle: const TextStyle(
          color: Colors.white,
          fontSize: 30,
        ),
        amPmDigitTextStyle: const TextStyle(
            color: Colors.blueGrey, fontWeight: FontWeight.bold),
      ),
      settingsButton(context),
    ],
  );
}

Widget settingsButton(BuildContext context) {
  return IconButton(
    icon: const Icon(Icons.settings),
    onPressed: () {
      // show settings screen.
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return const Settings();
        },
      );
    },
  );
}

class OvenWarmUpProgress extends StatefulWidget {
  const OvenWarmUpProgress({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return WarmUpProgressState();
  }
}

class WarmUpProgressState extends State<OvenWarmUpProgress> {
  int totalTime = 600;
  int currentTime = 100;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 10),
        const Text("Opwarm Status:"),
        const SizedBox(height: 5),
        LinearProgressIndicator(
          value: 1 * (currentTime / totalTime),
          backgroundColor: Colors.grey,
          valueColor: const AlwaysStoppedAnimation<Color>(Colors.red),
          minHeight: 10,
        ),
        const Text("nog --:--"),
        const SizedBox(height: 10)
      ],
    );
  }
}
