import 'package:flutter/material.dart';
import 'package:oven_timer/bottombar.dart';
import 'package:oven_timer/header.dart';
import 'package:oven_timer/level.dart';
import 'package:oven_timer/volume_warning.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    //debugPaintSizeEnabled = true;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Oven Timer',
      theme: ThemeData(primarySwatch: Colors.green),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    Widget onVolumeSpace;
    try {
      onVolumeSpace = const VolumeWarning();
    } catch (e) {
      onVolumeSpace = Container();
    }

    return Scaffold(
      appBar: mainAppBar(context),
      body: Column(
        children: [
          const OvenWarmUpProgress(),
          const Expanded(flex: 1, child: Level(row: 1)),
          const Expanded(flex: 1, child: Level(row: 2)),
          const Expanded(flex: 1, child: Level(row: 3)),
          const Expanded(flex: 1, child: Level(row: 4)),
          const Expanded(flex: 1, child: Level(row: 5)),
          const Expanded(flex: 1, child: Level(row: 6)),
          onVolumeSpace,
        ],
      ),
      bottomNavigationBar: const BottomMainBar(),
    );
  }
}
