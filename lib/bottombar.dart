import 'package:flutter/material.dart';

class BottomMainBar extends StatefulWidget {
  const BottomMainBar({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return BottomMainBarState();
  }
}

class BottomMainBarState extends State<BottomMainBar> {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: 30,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            alignment: Alignment.centerLeft,
            child: Row(children: [
              const Text("180 ºc"),
              Switch(
                value: isSwitched,
                onChanged: (value) {
                  setState(() {
                    isSwitched = value;
                  });
                },
              ),
              const Text("200 ºc"),
            ]),
          ),
          Container(
            height: 40,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            alignment: Alignment.centerRight,
            child: const ElevatedButton(
              onPressed: null,
              child: Icon(Icons.power),
            ),
          ),
        ],
      ),
    );
  }
}
